root = this
$ = jQuery
hb = Handlebars

root.cart = do ($, hb) ->
	model_type =  [ { id: 0, name: "Galletas" }
	                { id: 1, name: "Pastas" }
	                { id: 2, name: "Other" }
	              ]
	model_item =  [ { id: 0, type_id: 0, price: 4, name: "Oreo", 				description: "Yumy oreos" }
	                { id: 1, type_id: 0, price: 5, name: "Chips Ahoy!", 		description: "Yummy cookies" }
	                { id: 2, type_id: 1, price: 3, name: "Other Cookie Brand",	description: "Yummm!" }
	              ]

	types_ul	= $('#types').find 'ul'
	cart_ul		= $('#cart').find 'ul'
	items_ul 	= $('#items').find 'ul'
	no_items_li = $('li').text 'Sorry, no items to display!'
	cart_ul_parent = cart_ul.parent()
	cart_total = cart_ul_parent.find 'h3 span'
	types_template 		= hb.compile $("#types-template").html()
	items_template		= hb.compile $("#items-template").html()
	item_cart_template	= hb.compile $("#item-cart-template").html()

	_refresh = ->
		types_ul.empty()
		types_ul.append types_template({type: model_type})

	#Clicked to select a Type
	types_ul.on('click', 'li', ->

		$this = $(@)
		id  = +$this.attr 'id'
		current_type = model_type[id]
		display_items = []

		console.log 'clicked type', id, current_type.name

		$('div#types .active').removeClass()
		$this.toggleClass 'active'

		items_ul.parent().find('h3').text 'Items in ' + current_type.name

		items_ul.empty()

		$.map(model_item, (current_item) ->
			if current_item.type_id is id then display_items.push current_item
		)

		items_ul.append items_template({item: display_items})

		if items_ul.find('li').length < 1 then items_ul.append no_items_li
	)

	#Clicked to Buy a new Item
	items_ul.on('click', 'li', ->
		$this = $(@)
		id = $this.find('a').attr 'id'
		items = cart_ul_parent.find('strong').length + 1 #+1 because we are adding a new item

		console.log 'clicked to buy', id, model_item[id].name

		cart_ul.append item_cart_template(model_item[id])

		total = 0
		for i in [0..items]
			total += +cart_ul_parent.find('strong').eq(i).text()
		
		cart_ul.parent().find('#price').text total

		if total > 0 then cart_ul.siblings().find('a').removeClass 'disabled'
		
		cart_total.text +cart_total.text() + 1
	)

	#Clicked to Remove an Item
	cart_ul.on('click', 'li', ->
		$this = $(@)
		id = $this.find('a').attr 'id'
		price_label = $('#cart').find '#price'
		cart_total = cart_ul.parent().find 'h3 span'
		remove_amount = +$this.find('strong').text()
		current_price = +price_label.text()
		total = current_price - remove_amount

		console.log 'clicked remove', id, model_item[id].name, 'rm', remove_amount, 'current_price', current_price, 'total', total

		price_label.text total

		$this.remove()

		if total < 1 then cart_ul.siblings().find('a').addClass 'disabled'

		cart_total.text +cart_total.text() - 1
	)

	pub =
		init: () -> _refresh()
		
cart.init()