/**********************************************************************************
 *								CANASTA NAMESPACE
 **********************************************************************************/
var CANASTA = CANASTA || {};

CANASTA.namespace = function (ns_string) {
	"use strict";

	var parts = ns_string.split('.'),
		parent = CANASTA,
		i;

	// strip redundant leading global
	if (parts[0] === "CANASTA") {
		parts = parts.slice(1);
	}

	for (i = 0; i < parts.length; i += 1) {
		// create a property if it doesn't exist
		if (typeof parent[parts[i]] === "undefined") {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}

	return parent;
};

/**********************************************************************************
 *								CANASTA.Cart
 **********************************************************************************/

CANASTA.namespace('CANASTA.Cart');
CANASTA.Cart = (function ($, hb) {
	//dependencies: JQuery 1.7.2, Handlebars.js 1.0.beta.6
	
	//MODELS:
	var model_type = [{
		id : 0,
		name : "Galletas"
	},
	{
		id : 1,
		name : "Pastas"
	},
	{
		id: 2,
		name : "Other food type"
	}];

	var model_item = [{
		id : 0,
		type_id : 0,
		price: 4,
		name : "Oreo",
		description : "best cookies ever"
	},
	{
		id : 1,
		type_id : 0,
		price: 5,
		name : "Chips Ahoy!",
		description : "moar cookies"
	},
	{
		id : 2,
		type_id : 1,
		price: 3,
		name : "Some Pasta",
		description : "Some description..."
	}];


	//private variables
	var types_ul = $('#types').find('ul'),
		cart_ul = $('#cart').find('ul'),
		items_ul = $('#items').find('ul'),
		no_items_li = $('li').text('Sorry, no items to display!'),
		types_template = hb.compile($("#types-template").html()),
		items_template = hb.compile($("#items-template").html()),
		item_cart_template = hb.compile($("#item-cart-template").html()),

	//private functions
	_refresh_types = function () {

		types_ul.empty();

		types_ul.append(types_template({type: model_type}));

	};

	//one-time initialization procedures

	//Clicked to select a Type
	types_ul.on('click', 'li', function () {
		var $this = $(this),
			id = +$this.attr('id'),
			current_type = model_type[id],
			display_items = [];

		console.log('clicked type', id, current_type.name);

		types_ul.find('.active').removeClass('active');
		$this.addClass('active');

		items_ul.parent().find('h3').text('Items in ' + current_type.name);

		items_ul.empty();

		$.map(model_item, function (current_item) {
			if (current_item.type_id === id) {
				display_items.push(current_item);
			}
		});

		items_ul.append(items_template({item: display_items}));

		if (items_ul.find('li').length < 1) {
			items_ul.append(no_items_li);
		}

	});//types_ul

	//Clicked to Buy a new Item
	items_ul.on('click', 'li', function () {
		var $this = $(this),
			id = $this.find('a').attr('id');

		if (id) {
			var current_item = model_item[id],
				cart_ul_parent = cart_ul.parent(),
				cart_total = cart_ul_parent.find('h3 span'),
				total = 0,
				i = 0,
				l = cart_ul_parent.find('strong').length + 1; //+1 because we are adding a new item

			console.log('clicked to buy', id, model_item[id].name);

			cart_ul.append(item_cart_template(current_item));

			for (i = 0; i < l ; i+=1) {
				total += +(cart_ul_parent.find('strong').eq(i).text());
			}

			cart_ul.parent().find('#price').text(total);

			if (total > 0) {
				cart_ul.siblings().find('a').removeClass('disabled');
			}

			cart_total.text(+cart_total.text() + 1);
		}

	});//items_ul
	
	//Clicked to Remove an Item
	cart_ul.on('click', 'li', function () {
		var $this = $(this),
			id = $this.find('a').attr('id'),
			price_label = $('#cart').find('#price'),
			cart_total = cart_ul.parent().find('h3 span'),
			remove_amount = +$this.find('strong').text(),
			current_price = +price_label.text(),
			total = current_price - remove_amount;

		console.log('clicked remove', id, model_item[id].name, 'rm', remove_amount, 'current_price', current_price, 'total', total);

		price_label.text(total);

		$this.remove();

		if (total < 1) {
			cart_ul.siblings().find('a').addClass('disabled');
		}

		cart_total.text(+cart_total.text() - 1);

	});//cart_ul

	//public API
	return {
		init : _refresh_types
	};

}(jQuery, Handlebars)); //CANASTA.Cart

CANASTA.Cart.init();